﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UserRegistrationAssignment_V1.ViewModels;
using UserRegistrationAssignment_V1.Models;
using UserRegistrationAssignment_V1.UserModelContext;
using UserRegistrationAssignment_V1.BAL;
using System.Net.Mail;
using System.Net;
using System.Xml.Linq;
using System.Text.RegularExpressions;

namespace UserRegistrationAssignment_V1.Controllers
{
    public class RegistrationController : Controller
    {
        // GET: Registration
        public ActionResult Register()
        {
            return View();
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Register(UserRegistration userAccount)
        {
            string message = "";
            bool status = false;
            if (!ModelState.IsValid)
            {
                return View(userAccount);
            }
            using (UserAccountContext userDB = new UserAccountContext())
            {
                if (IsEmailExist(userAccount) != null)
                {
                    ViewBag.invalidEmailMessage = "Email id is already Exit";
                    return View(userAccount);
                }
                BusnissesLogics.ContactTypeIsEmpty(userDB);
                
                var uploadDir = "~/Images";
                var fileName = userAccount.EmailId + userAccount.ImageUpload.FileName;
                var imagePath = Path.Combine(Server.MapPath(uploadDir), fileName);
                var imageUrl = Path.Combine(uploadDir, fileName);
                userAccount.ImageUpload.SaveAs(imagePath);

                User userModel=BusnissesLogics.UploadUserData( userDB,  userAccount, imageUrl);
                
                for (int i=0;i< userAccount.Contact.Count();i++)
                {
                    var userContactModel = new UserContact
                    {
                        ContactNumber = userAccount.ContactNumber[i],
                        ContactId = (int)userAccount.Contact[i],
                        Id = userModel.Id
                    };
                    userDB.RelationshipHolds.Add(userContactModel);
                }
                userDB.SaveChanges();
               if(ContactValidation(userAccount)!=0)
                {
                    ViewBag.ContactError = "Invalid input";
                    return View(userAccount); 
                }
                SendVarificationCode(userModel.Email, userModel.ActivationCode.ToString());
                message = "Registered successfully done . Account Activation link "+
                    "has been sent to your email id : "+userModel.Email;
                status = true;
                ModelState.Clear();

                 ViewBag.successMessage = message;
                ViewBag.status = status;

            }
            return View(userAccount);
        }
        [NonAction]
        public void SendVarificationCode(string emailId, string activationCode)
        {
            var verifyUrl = "/Login/UserLogin" + activationCode;
            var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyUrl);
            var fromEmail = new MailAddress("mindfiresolutionspurnendu@gmail.com","DotNet MVC");
            var toEmail = new MailAddress(emailId);
            var fromEmailPassword = "mindfire@123";
            string subject = "Your account is successfully created";
            string body = "<br/><br/>We are excited to tell you that your MVC account is"+
                "successfully created . Please click on below link to verify your account"+
                "<br/><br/><a href = '" + link+"'>"+link+"</a>";
            var smtp = new SmtpClient
            {
                  Host = "smtp.gmail.com",
                  Port = 587,
                  EnableSsl =true,
                  DeliveryMethod = SmtpDeliveryMethod.Network,
                  UseDefaultCredentials = false,
                  Credentials = new NetworkCredential(fromEmail.Address,fromEmailPassword)
            };
            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
                smtp.Send(message);
        }
        [HttpPost]
        public ActionResult ValidateEmail()
        {
            var email = Request["email"];
            using (UserAccountContext userDB = new UserAccountContext())
            {
                var isExistEmail = userDB.UserDetails.Any(x => x.Email == email);
                if (isExistEmail)
                {
                    return Json("1");
                }
                return Json("0");
            }
        }
        [NonAction]
        public User IsEmailExist(UserRegistration userAccount)
        {
            var email = Request["email"];
            using (UserAccountContext userDB = new UserAccountContext())
            {
                var userExist = userDB.UserDetails.FirstOrDefault(x => x.Email == userAccount.EmailId);
                return userExist;
            }
        }
        [NonAction]
        public int ContactValidation(UserRegistration userAccount)
        {
            Regex rx = new Regex(@"^[0-9]*$");
            for (int i=0;i< userAccount.Contact.Count();i++)
            {
                if(!(rx.IsMatch(userAccount.ContactNumber[i])))
                {
                    return 1 ;
                }
            }
            return 0;
        }
    }
}
