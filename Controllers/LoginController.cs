﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UserRegistrationAssignment_V1.ViewModels;
using UserRegistrationAssignment_V1.UserModelContext;
using System.Web.Security;

namespace UserRegistrationAssignment_V1.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult UserLogin()
        {
            return View();
        }
        [HttpPost]
        public ActionResult UserLogin(UserLogin userLogin)
        {
            if (!ModelState.IsValid)
            {
                return View(userLogin);
            }
            using (UserAccountContext obj=new UserAccountContext())
            {
                string pass = PasswordClass.Encrypt(userLogin.Password);
                var userData = obj.UserDetails.FirstOrDefault(x => x.Email == userLogin.EmailId && x.Password == pass);
                if (userData != null)
                {
                    Session["email"] = userLogin.EmailId;
                    Session["password"] = pass;
                    ViewBag.messageLogin = "Login successful";
                    return RedirectToAction("UserDetails", "Home");
                }
                else
                {
                    ViewBag.messageLogin = "Invalid email or password";
                }
            }
            return View();
        }
        public ActionResult UserLogout()
        {
            
            Session["email"] = null;
            Session["password"] = null;
            return RedirectToAction("UserLogin");
        }
        public ActionResult ChangePassword()
        {
            return View();
        }

    }
}