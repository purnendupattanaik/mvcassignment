﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using UserRegistrationAssignment_V1.ViewModels;
using UserRegistrationAssignment_V1.Models;
using UserRegistrationAssignment_V1.UserModelContext;
using System.Data;
using System.Data.Entity;

namespace UserRegistrationAssignment_V1.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Details()
        {
            if (Session["email"] != null && Session["password"] != null)
            {
                using (UserAccountContext userAccount = new UserAccountContext())
                {
                    return View(userAccount.UserDetails.ToList<User>());
                }
            }
            return RedirectToAction("UserLogin", "Login");
        }
        public ActionResult UserDetails()
        {
            string email = Convert.ToString(Session["email"]);
            if (Session["email"] != null && Session["password"] != null)
            {
                using (UserAccountContext userAccount = new UserAccountContext())
                {
                    var obj = userAccount.UserDetails.Where(m=>m.Email==email).FirstOrDefault();
                    var userContactData = userAccount.RelationshipHolds.Where(m => m.Id == obj.Id).Include(m =>m.UserContactData).ToList();
                    var userDetailsObj = new UserDetailsView
                    {
                        FirstName = obj.FirstName,
                        LastName = obj.LastName,
                        Mobile = obj.Mobile,
                        Email = obj.Email,
                        Address = obj.Address,
                        ImageUrl = obj.ImageUrl,
                        AlternetText=obj.AlternetText,
                        UserContactList = userContactData
                    };
                    return View(userDetailsObj);
                }
            }
            return RedirectToAction("UserLogin", "Login");
        }
    }
}
