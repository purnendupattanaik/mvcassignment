namespace UserRegistrationAssignment_V1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class account : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "IsEmailVarified", c => c.Boolean(nullable: false));
            AddColumn("dbo.Users", "ActivationCode", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "ActivationCode");
            DropColumn("dbo.Users", "IsEmailVarified");
        }
    }
}
