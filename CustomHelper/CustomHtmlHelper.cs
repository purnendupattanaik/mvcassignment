﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UserRegistrationAssignment_V1.CustomHelper
{
    public static class CustomHtmlHelper
    { 
        public static IHtmlString Image(this HtmlHelper helper,string src,string alt,string height="40%",string width="40%")
        {
            TagBuilder tb = new TagBuilder("img");
            tb.Attributes.Add("src", VirtualPathUtility.ToAbsolute(src));
            tb.Attributes.Add("alt",alt);
            tb.Attributes.Add("height", height);
            tb.Attributes.Add("width", width);
            return new MvcHtmlString(tb.ToString(TagRenderMode.SelfClosing));
        }
    }
}