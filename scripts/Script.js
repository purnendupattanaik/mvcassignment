﻿var cloneCount = 1;
$(document).ready(function () {
    
  //  var elementGenerator = '<input class="form-control" name="ContactNumber" placeholder="Enter Your Number" type="text" value=""><span class="col-md-8 error"></span>';
    
    $("#AddContact").click(function () {
        $("#CloneItem").clone().attr('id', '' + cloneCount).append('<input class="form-control" name="ContactNumber" placeholder="Enter Your Number" type="text" value="" id="AltContactNumbers' + cloneCount + '" oninput="contactValid(' + cloneCount + ')"><span class="col-md-8 error" id="errorMessage' + cloneCount + '"></span>').insertBefore("#AppendItem");
        cloneCount++;
    });
    $("#ContactNumber").on("input", function () {
        
        var firstContactData = $("#ContactNumber").val();
        var firstError = "#FirstErrorContactNumber";
        checkContact(firstContactData, firstError);
    });
   
});

function checkContact(data, error) {
    var regex = /^[0-9]*$/;
    if (!(regex.test(data))) {
        $(error).html("Enter only numbers");
    }
    else {
        $(error).html("");
    }
}
function contactValid(cloneCount) {
    for (var i = 1; i <= cloneCount; i++) {
        var contactValue = $("#AltContactNumbers" + i).val();
        var message = "#errorMessage" + i;
        checkContact(contactValue, message);
    }
}
