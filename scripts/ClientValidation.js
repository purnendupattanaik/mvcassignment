﻿$(function () {

    $("#ErrorFirstName").hide();
    $("#ErrorLastName").hide();
    $("#ErrorEmailId").hide();
    $("#ErrorPassword").hide();
    $("#ErrorConfirmPassword").hide();
    $("#ErrorMobile").hide();
    $("#ErrorAddress").hide();
    $("#ErrorImageUpload").hide();

    $("#FirstName").focusout(function () {
        checkName("#FirstName", "#ErrorFirstName", "first name");
    });
    $("#LastName").focusout(function () {
        checkName("#LastName", "#ErrorLastName", "last name");
    });
    $("#EmailId").focusout(function () {
        checkEmail("#EmailId", "#ErrorEmailId");
    });
    $("#Password").focusout(function () {
        checkPassword("#Password", "#ErrorPassword");
    });
    $("#ConfirmPassword").focusout(function () {
        checkConfirmPassword("#ConfirmPassword", "#Password", "#ErrorConfirmPassword");
    });
    $("#Mobile").focusout(function () {
        checkMobile("#Mobile", "#ErrorMobile");
    });
    $("#Address").focusout(function () {
        checkAddress("#Address", "#ErrorAddress");
    });
    function checkName(dataId, errorId, name) {
        var regex = /^[a-zA-Z]+$/;
        var nameLength = $(dataId).val().length;
        if (nameLength < 1) {
            $(errorId).html("You must provide a " + name);
            $(errorId).show();
        }
        else if (nameLength > 20) {
            $(errorId).html("Should be less than 20 character");
            $(errorId).show();
        }
        else if (!(regex.test($(dataId).val()))) {
            $(errorId).html("Use only alphabets");
            $(errorId).show();
        }
        else {
            $(errorId).hide();
        }
    }
    function checkEmail(dataId, errorId) {
        var regex = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
        var email = $(dataId).val();
        var emailLength = $(dataId).val().length;
        if (emailLength < 1) {
            $(errorId).html("You must provide a email address");
            $(errorId).show();
        }
        else if (!(regex.test($(dataId).val()))) {
            $(errorId).html("Invalid email");
            $(errorId).show();
        }
        else {
            $(errorId).hide();
        }
        var validemail = new FormData();
        validemail.append("email", email);
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "ValidateEmail",
            data: validemail,
            //data: JSON.stringify({ 'email' : email}),
            contentType: false,
            processData: false,
            success: function (data) {
                if (data == "1") {
                    $(errorId).html("This email address is already exist");
                    $(errorId).css("color", "red");
                    $(errorId).show();
                }
                else {
                    $(errorId).html("Email is available");
                    $(errorId).show().css("color","green");
                }
            },
            error: function (a, jqXHR, exception) { }
        });
    }

    function checkPassword(dataId, errorId) {
        var uppercaseReg = /[A-Z]/;
        var lowercaseReg = /[a-z]/;
        var numberReg = /[0-9]/;
        var symbolReg = /[#?!@$%^&*-]/;
        var passwordLength = $(dataId).val().length;
        if (passwordLength < 8) {
            $(errorId).html("Password must be at least 8 characters");
            $(errorId).show();
        }
        else if (!numberReg.test($(dataId).val())) {
            $(errorId).html("Password must contain at least one number");
            $(errorId).show();
        }
        else if (!uppercaseReg.test($(dataId).val())) {
            $(errorId).html("Password must contain at least one upper case");
            $(errorId).show();
        }
        else if (!lowercaseReg.test($(dataId).val())) {
            $(errorId).html("Password must contain at least one lower case");
            $(errorId).show();
        }
        else if (!symbolReg.test($(dataId).val())) {
            $(errorId).html("Password must contain at least one symbol");
            $(errorId).show();
        }
        else {
            $(errorId).hide();
        }
    }
    function checkConfirmPassword(dataId, compareData, errorId) {
        var password = $(compareData).val();
        var confirmPassword = $(dataId).val();
        if (password !== confirmPassword) {
            $(errorId).html("The password and confirmation password do not match");
            $(errorId).show();
        }
        else {
            $(errorId).hide();
        }
    }
    function checkMobile(dataId, errorId) {
        var regex = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
        var mobileLength = $(dataId).val().length;
        if (mobileLength < 1) {
            $(errorId).html("You must provide a phone number");
            $(errorId).show();
        }
        else if (mobileLength > 10) {
            $(errorId).html("Mobile number must be only 10 numbers");
            $(errorId).show();
        }
        else if (!(regex.test($(dataId).val()))) {
            $(errorId).html("Invalid mobile number");
            $(errorId).show();
        }
        else {
            $(errorId).hide();
        }
    }
    function checkAddress(dataId, errorId) {
        var addressLength = $(dataId).val().length;
        if (addressLength < 1) {
            $(errorId).html("You must provide a address");
            $(errorId).show();
        }
        else {
            $(errorId).hide();
        }
    }
    $("#ImageUpload").change(function () {

        var val = $(this).val();

        switch (val.substring(val.lastIndexOf('.') + 1).toLowerCase()) {
            case 'gif': case 'jpg': case 'png': case 'jpeg':
                $("#ErrorImageUpload").hide();
                break;
            default:
                $("#ErrorImageUpload").html("Not a image file");
                $("#ErrorImageUpload").show();
                $(this).val('');
                break;
        }
    });


});