﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using UserRegistrationAssignment_V1.ViewModels;
using UserRegistrationAssignment_V1.Models;

namespace UserRegistrationAssignment_V1.UserModelContext
{
    public class UserAccountContext : DbContext
    {
        public UserAccountContext() :base("UserAccount")
        {

        }
        public DbSet<User> UserDetails { get; set; }
        public DbSet<UserContactType> UserContactDetails { get; set; }
        public DbSet<UserContact> RelationshipHolds { get; set; }
    }
}