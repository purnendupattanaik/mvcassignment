﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UserRegistrationAssignment_V1.ViewModels;
using UserRegistrationAssignment_V1.Models;
using UserRegistrationAssignment_V1.UserModelContext;
using System.Text.RegularExpressions;

namespace UserRegistrationAssignment_V1.BAL
{
    public class BusnissesLogics
    {
        public static void ContactTypeIsEmpty(UserAccountContext userDB)
        {
            if(userDB.RelationshipHolds.Count()==0)
                {
                    foreach(string name in Enum.GetNames(typeof(Contact)))
                    {
                        var userContactType = new UserContactType
                        {
                            Type= name
                        };
                        userDB.UserContactDetails.Add(userContactType);
                    }
                    userDB.SaveChanges();
                }
        }
        public static User UploadUserData(UserAccountContext userDB, UserRegistration userAccount, string imageUrl)
        {
            var userModel = new User
            {
                ActivationCode = Guid.NewGuid(),
                ImageUrl = imageUrl,
                FirstName = userAccount.FirstName,
                LastName = userAccount.LastName,
                Password = PasswordClass.Encrypt(userAccount.Password),
                Email = userAccount.EmailId,
                Address = userAccount.Address,
                Mobile = userAccount.Mobile,
                AlternetText = userAccount.FirstName + " " + userAccount.LastName + "'s Photo"
            };
            userDB.UserDetails.Add(userModel);
            userDB.SaveChanges();
            return userModel;
        }
    }
}