﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UserRegistrationAssignment_V1.ViewModels
{
    public class PasswordReset
    {
        [Required]
        [EmailAddress]
        public string EmailId { get; set; }
    }
}