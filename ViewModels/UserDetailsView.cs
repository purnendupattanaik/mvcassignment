﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UserRegistrationAssignment_V1.Models;

namespace UserRegistrationAssignment_V1.ViewModels
{
    public class UserDetailsView
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        
        public string Mobile { get; set; }

        public string Email { get; set; }

        public List<UserContact> UserContactList { get; set; }

        public string Address { get; set; }

        public string ImageUrl { get; set; }
        public string AlternetText { get; set; }

    }
}