﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UserRegistrationAssignment_V1.ViewModels
{
    public class UserRegistration
    {
        [DisplayName("First Name *")]
        [Required(ErrorMessage = "You must provide a first name")]
        [RegularExpression("^([a-zA-Z]+)$", ErrorMessage = "Use only alphabets")]
        public string FirstName { get; set; }

        [DisplayName("Last Name *")]
        [Required(ErrorMessage = "You must provide a last name")]
        [RegularExpression("^([a-zA-Z]+)$", ErrorMessage = "Use only alphabets")]
        public string LastName { get; set; }

        [DisplayName("Email Id *")]
        [Required(ErrorMessage = "You must provide a email address")]
        [EmailAddress]
        public string EmailId { get; set; }

        [DisplayName("Password *")]
        [Required(ErrorMessage = "You must provide a password")]
        [RegularExpression("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$", ErrorMessage = "Invalid Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DisplayName("Confirm Password *")]
        [Required(ErrorMessage = "You must provide the same password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        [DisplayName("Mobile *")]
        [Required(ErrorMessage = "You must provide a phone number")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Invalid mobile number")]
        public string Mobile { get; set; }

        public Contact [] Contact {get;set;}
        public string[] ContactNumber { get; set; }

        [DisplayName("Address *")]
        [Required(ErrorMessage = "You must provide a address")]
        public string Address { get; set; }

        [Required(ErrorMessage = "You must provide an image")]
        [DataType(DataType.Upload)]
        public HttpPostedFileBase ImageUpload { get; set; }

    }
    public enum Contact
    {
        alternateMobile=1,
        home=2,
        office =3,
        fax =4
    }
}